﻿namespace Thucook.Commons.Enums
{
    public enum UserTypeEnum
    {
        SuperAdmin = 1,
        CrmUser = 2,
        EndUser = 3
    }
}
