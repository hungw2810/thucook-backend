﻿namespace Thucook.Commons.Enums
{
    public enum MedicineUnitTypeEnum
    {
        Vỉ = 1,
        Hộp = 2,
        Gói = 3,
        Tuýp = 4,
        Chai = 5,
    }
}
