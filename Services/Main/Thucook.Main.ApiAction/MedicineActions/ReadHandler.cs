﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.Commons.Enums;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Medicine;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.MedicineActions
{
    public class ReadHandler : IRequestHandler<ApiActionLocationRequest<MedicineReadInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public ReadHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<MedicineReadInputModel> request, CancellationToken cancellationToken)
        {
            var medicine = await (from m in _dbContext.Medicines
                                  where
                                  m.LocationId == request.LocationId &&
                                  m.MedicineId == request.Input.MedicineId &&
                                  !m.IsDeleted
                                  select new MedicineResponseModel
                                  {
                                      MedicineId = m.MedicineId,
                                      MedicineName = m.MedicineName,
                                      MedicineUnitTypeId = (MedicineUnitTypeEnum)m.MedicineUnitTypeId,
                                      Quantity = m.Quantity
                                  }).FirstOrDefaultAsync(cancellationToken);
            if (medicine == null)
            {
                return ApiResponse.CreateErrorModel(System.Net.HttpStatusCode.BadRequest, ApiInternalErrorMessages.MedicineNotFound);
            }

            return ApiResponse.CreateModel(medicine);
        }
    }
}

