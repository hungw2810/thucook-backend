﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Medicine;

namespace Thucook.Main.ApiAction.MedicineActions
{
    public class DeleteHandler : IRequestHandler<ApiActionLocationRequest<MedicineDeleteInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public DeleteHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<MedicineDeleteInputModel> request, CancellationToken cancellationToken)
        {
            var medicine = await (from m in _dbContext.Medicines
                                  where
                                  m.LocationId == request.LocationId &&
                                  m.MedicineId == request.Input.MedicineId &&
                                  m.IsDeleted == false
                                  select m).FirstOrDefaultAsync(cancellationToken);
            if (medicine == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.MedicineNotFound);
            }

            medicine.IsDeleted = true;
            medicine.UpdatedAt = DateTime.Now;
            medicine.UpdatedByUserId = request.UserId;

            _dbContext.Medicines.Update(medicine);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
