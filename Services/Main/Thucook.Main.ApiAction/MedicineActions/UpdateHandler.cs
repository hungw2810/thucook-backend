﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Medicine;

namespace Thucook.Main.ApiAction.MedicineActions
{
    public class UpdateHandler : IRequestHandler<ApiActionLocationRequest<MedicineUpdateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public UpdateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<MedicineUpdateInputModel> request, CancellationToken cancellationToken)
        {
            var medicine = await (from m in _dbContext.Medicines
                                  where
                                  m.LocationId == request.LocationId &&
                                  m.MedicineId == request.Input.MedicineId &&
                                  !m.IsDeleted
                                  select m).FirstOrDefaultAsync(cancellationToken);
            if (medicine == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.MedicineNotFound);
            }

            var existedMedicine = await (from m in _dbContext.Medicines
                                         where
                                         m.LocationId == request.LocationId &&
                                         m.MedicineId != request.Input.MedicineId &&
                                         m.MedicineName == request.Input.Details.MedicineName &&
                                         !m.IsDeleted
                                         select m)
                                         .FirstOrDefaultAsync(cancellationToken);
            if (existedMedicine != null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateName);
            }

            medicine.MedicineName = request.Input.Details.MedicineName;
            medicine.MedicineUnitTypeId = (short)request.Input.Details.MedicineUnitTypeId;
            medicine.Quantity = request.Input.Details.Quantity;
            medicine.UpdatedAt = DateTime.Now;
            medicine.UpdatedByUserId = request.UserId;

            _dbContext.Medicines.Update(medicine);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
