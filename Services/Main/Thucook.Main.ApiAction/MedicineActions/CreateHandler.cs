﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Medicine;

namespace Thucook.Main.ApiAction.MedicineActions
{
    public class CreateHandler : IRequestHandler<ApiActionLocationRequest<MedicineCreateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public CreateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<MedicineCreateInputModel> request, CancellationToken cancellationToken)
        {
            // Check duplicate medicine name:
            var existedMedicine = await (from m in _dbContext.Medicines
                                         where
                                         m.LocationId == request.LocationId &&
                                         m.MedicineName == request.Input.MedicineName &&
                                         m.MedicineUnitTypeId == (short)request.Input.MedicineUnitTypeId &&
                                         !m.IsDeleted
                                         select m)
                                         .FirstOrDefaultAsync(cancellationToken);
            if (existedMedicine != null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateName);
            }

            var medicine = new Medicine
            {
                LocationId = request.LocationId,
                MedicineName = request.Input.MedicineName,
                MedicineUnitTypeId = (short)request.Input.MedicineUnitTypeId,
                Quantity = request.Input.Quantity,
                IsEditable = true,
                CreatedAt = DateTime.Now,
                CreatedByUserId = request.UserId
            };

            _dbContext.Medicines.Add(medicine);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
