﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thucook.Commons.Enums;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Medicine;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.MedicineActions
{
    public class SearchHandler : IRequestHandler<ApiActionLocationRequest<MedicineSearchInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SearchHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<MedicineSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = (from m in _dbContext.Medicines
                         where
                         m.IsDeleted == false
                         select new
                         {
                             m.MedicineId,
                             m.MedicineName,
                             m.MedicineUnitTypeId,
                             m.Quantity,
                             m.IsEditable
                         })
                        .Distinct();

            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Match(item.MedicineName, $"{request.Input.Keyword}*", MySqlMatchSearchMode.Boolean)
                    select item;
            }
            // Page info
            var totalItems = await query.CountAsync(cancellationToken);
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);
            // Order
            query = query
                .OrderBy(u => u.MedicineName);
            // Page data
            var doctors = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                doctors.Select(d => new MedicineResponseModel
                {
                    MedicineId = d.MedicineId,
                    MedicineName = d.MedicineName,
                    MedicineUnitTypeId = (MedicineUnitTypeEnum)d.MedicineUnitTypeId,
                    Quantity = d.Quantity,
                    IsEditable = d.IsEditable
                }).ToList(),
                requestPaging);
        }
    }
}
