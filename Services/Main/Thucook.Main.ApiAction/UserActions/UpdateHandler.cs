﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.User;

namespace Thucook.Main.ApiAction.UserActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<UserUpdateInfoInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public UpdateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<UserUpdateInfoInputModel> request, CancellationToken cancellationToken)
        {
            var userInfo = await (from u in _dbContext.Users
                                  join ui in _dbContext.UserInformations on u.UserId equals ui.UserId
                                  where
                                  u.UserId == request.UserId &&
                                  !u.IsDeleted
                                  select ui).FirstOrDefaultAsync(cancellationToken);
            if (userInfo == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.UserNotFound);
            }

            userInfo.FirstName = request.Input.Details.FirstName;
            userInfo.LastName = request.Input.Details.LastName;
            userInfo.Email = request.Input.Details.Email;
            userInfo.PhoneNumber = request.Input.Details.PhoneNumber;
            userInfo.Birthday = request.Input.Details.BirthDayUnixTime.HasValue ? request.Input.Details.BirthDayUnixTime.Value.ToDateTimeFromUnixTime() : null;
            userInfo.Gender = (short)request.Input.Details.Gender;
            userInfo.CityId = (short)request.Input.Details.CityId;
            userInfo.Address = request.Input.Details.Address;

            _dbContext.UserInformations.Update(userInfo);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}

