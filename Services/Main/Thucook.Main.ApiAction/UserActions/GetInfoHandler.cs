﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thucook.Commons.Enums;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.User;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.UserActions
{
    public class GetInfoHandler : IRequestHandler<ApiActionAuthenticatedRequest<UserGetInfoInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public GetInfoHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<UserGetInfoInputModel> request, CancellationToken cancellationToken)
        {
            var user = await (from u in _dbContext.Users
                              join i in _dbContext.UserInformations on u.UserId equals i.UserId
                              where
                              u.UserId == request.UserId &&
                              u.IsDeleted == false
                              select new UserInfoResponseModel
                              {
                                  UserId = request.UserId,
                                  UserName = u.UserName,
                                  FirstName = i.FirstName,
                                  LastName = i.LastName,
                                  PhoneNumber = i.PhoneNumber,
                                  Email = i.Email,
                                  BirthdayUnix = i.Birthday.HasValue ? i.Birthday.Value.ToUnixTime() : null,
                                  Gender = (GenderEnum)i.Gender,
                                  CityId = (CityEnum)i.CityId,
                                  Address = i.Address
                              }).FirstOrDefaultAsync(cancellationToken);

            return ApiResponse.CreateModel(user);
        }
    }
}
