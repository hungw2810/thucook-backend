﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Thucook.Commons.Extensions;
using Thucook.Commons.Utils;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Schedule;
using Thucook.Main.ApiService;

namespace Thucook.Main.ApiAction.ScheduleActions
{
    public class CreateHandler : IRequestHandler<ApiActionLocationRequest<ScheduleCreateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;
        private readonly IDoctorScheduleService _doctorScheduleService;

        public CreateHandler(ThucookContext dbContext, IDoctorScheduleService doctorScheduleService)
        {
            _dbContext = dbContext;
            _doctorScheduleService = doctorScheduleService;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<ScheduleCreateInputModel> request, CancellationToken cancellationToken)
        {
            var start = request.Input.StartDateTimeUnix.ToDateTimeFromUnixTime();
            var end = request.Input.EndDateTimeUnix.ToDateTimeFromUnixTime();
            var duration = end - start;

            #region Validate input
            //Check input start date must today or days in future
            if (start.Date < DateTime.Now.Date)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.PastTimeNotAllowed);
            }

            if (duration.TotalMilliseconds < 0 || duration.TotalMilliseconds > 24 * 60 * 60 * 1000)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ScheduleDurationTooLong);
            }

            var doctor = await (from d in _dbContext.Doctors
                                where
                                d.DoctorId == request.Input.DoctorId &&
                                d.IsDeleted == false &&
                                d.IsEnabled == true
                                select d).FirstOrDefaultAsync(cancellationToken);
            if (doctor == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DoctorNotFound);
            }

            // Check RecurrenceString
            try
            {
                var checkEvent = CalendarHelper.FromString(request.Input.RecurrenceString, start, end);
            }
            catch
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidRecurrenceString);
            }
            #endregion

            // New entity
            var newSchedule = new DoctorSchedule
            {
                DoctorId = request.Input.DoctorId,
                StartDatetime = start,
                EndDatetime = end,
                RecurrenceString = request.Input.RecurrenceString,
                ScheduleUntil = CalendarHelper.CalculateUntil(request.Input.RecurrenceString, start, end),
                IsDeleted = false,
                CreatedAt = DateTime.UtcNow,
                CreatedByUserId = request.UserId
            };

            // Check overlap schedule
            var hasOverlap = await _doctorScheduleService.CheckScheduleHasOverlap(newSchedule, null, cancellationToken);
            if (hasOverlap)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ScheduleOverlap);
            }

            _dbContext.DoctorSchedules.Add(newSchedule);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
