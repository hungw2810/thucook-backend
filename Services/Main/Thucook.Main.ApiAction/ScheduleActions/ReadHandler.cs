﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Schedule;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.ScheduleActions
{
    public class ReadHandler : IRequestHandler<ApiActionLocationRequest<ScheduleReadInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public ReadHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<ScheduleReadInputModel> request, CancellationToken cancellationToken)
        {
            var schedule = await (from s in _dbContext.DoctorSchedules
                                  join ds in _dbContext.DoctorSettings on s.DoctorId equals ds.DoctorId
                                  where
                                  s.ScheduleId == request.Input.ScheduleId
                                  select new
                                  {
                                      s.ScheduleId,
                                      s.DoctorId,
                                      s.StartDatetime,
                                      s.EndDatetime,
                                      s.RecurrenceString,
                                      ds.TimePerAppointmentInMinutes,
                                      ds.BufferTimePerAppointmentInMinutes,
                                      //s.Doctor.DoctorName,
                                      s.Doctor.Location.LocationName
                                  }).FirstOrDefaultAsync(cancellationToken);

            if (schedule == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ScheduleNotFound);
            }

            return ApiResponse.CreateModel(new ScheduleDetailsResponseModel
            {
                ScheduleId = schedule.ScheduleId,
                Doctor = new DoctorBaseResponseModel
                {
                    DoctorId = schedule.DoctorId,
                    //DoctorName = schedule.DoctorName,
                },
                LocationName = schedule.LocationName,
                StartDateTimeUnix = schedule.StartDatetime.ToUnixTime(),
                EndDateTimeUnix = schedule.EndDatetime.ToUnixTime(),
                RecurrenceString = schedule.RecurrenceString,
                SlotTimeSpan = schedule.TimePerAppointmentInMinutes + schedule.BufferTimePerAppointmentInMinutes
            });
        }
    }
}
