﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Doctor;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.DoctorActions
{
    public class ReadHandler : IRequestHandler<ApiActionLocationRequest<DoctorReadInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public ReadHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<DoctorReadInputModel> request, CancellationToken cancellationToken)
        {
            var doctor = await (from d in _dbContext.Doctors
                                where
                                d.LocationId == request.LocationId &&
                                d.IsDeleted == false &&
                                d.DoctorId == request.Input.DoctorId
                                select d).FirstOrDefaultAsync(cancellationToken);
            if (doctor == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DoctorNotFound);
            }

            var doctorSettings = await (from ds in _dbContext.DoctorSettings
                                        where
                                        ds.DoctorId == request.Input.DoctorId
                                        select ds).FirstOrDefaultAsync(cancellationToken);

            return ApiResponse.CreateModel(new DoctorResponseModel
            {
                DoctorId = doctor.DoctorId,
                DoctorName = doctor.DoctorName,
                DoctorCode = doctor.DoctorCode,
                PhoneNumber = doctor.PhoneNumber,
                Email = doctor.Email,
                YearsOfExperience = doctor.YearOfExperience,
                SpecialtyId = doctor.SpecialtyId,
                IsEnabled = doctor.IsEnabled,
                TimePerAppointmentInMinutes = doctorSettings.TimePerAppointmentInMinutes,
                BufferTimePerAppointmentInMinutes = doctorSettings.BufferTimePerAppointmentInMinutes,
                IsVisibleForBooking = doctorSettings.IsVisibleForBooking
            });

        }
    }
}
