﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Doctor;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.DoctorActions
{
    public class SearchHandler : IRequestHandler<ApiActionLocationRequest<DoctorSearchInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SearchHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<DoctorSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = (from d in _dbContext.Doctors
                         join sp in _dbContext.Specialties on d.SpecialtyId equals sp.SpecialtyId into spjn
                         from sp in spjn.DefaultIfEmpty()
                         where
                         d.LocationId == request.LocationId &&
                         sp.LocationId == request.LocationId &&
                         !d.IsDeleted && !sp.IsDeleted
                         select new
                         {
                             d.DoctorId,
                             d.DoctorCode,
                             d.DoctorName,
                             d.IsEnabled,
                             sp.SpecialtyName,
                             sp.SpecialtyShortName
                         })
                         .Distinct();
            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query = from item in query
                        where
                        EF.Functions.Like(item.DoctorName, $"{request.Input.Keyword}%") ||
                        EF.Functions.Like(item.DoctorCode, $"{request.Input.Keyword}%")
                        select item;
            }
            // Page info
            var totalItems = await query.CountAsync(cancellationToken);
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);
            // Order

            query = query
                .OrderBy(u => u.DoctorName);

            // Page data
            var doctors = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                doctors.Select(d => new DoctorSearchResponseModel
                {
                    DoctorId = d.DoctorId,
                    DoctorCode = d.DoctorCode,
                    DoctorName = d.DoctorName,
                    IsEnabled = d.IsEnabled,
                    SpecialtyName = d.SpecialtyName,
                    SpecialtyShortName = d.SpecialtyShortName
                }).ToList(),
                requestPaging);
        }
    }
}
