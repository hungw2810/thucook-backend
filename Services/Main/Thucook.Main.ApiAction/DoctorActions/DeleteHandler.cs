﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Doctor;

namespace Thucook.Main.ApiAction.DoctorActions
{
    public class DeleteHandler : IRequestHandler<ApiActionLocationRequest<DoctorDeleteInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public DeleteHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<DoctorDeleteInputModel> request, CancellationToken cancellationToken)
        {
            var doctor = await (from d in _dbContext.Doctors
                                where
                                d.LocationId == request.LocationId &&
                                d.IsDeleted == false &&
                                d.DoctorId == request.Input.DoctorId
                                select d).FirstOrDefaultAsync(cancellationToken);
            if (doctor == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DoctorNotFound);
            }

            doctor.IsDeleted = true;
            doctor.UpdatedAt = DateTime.Now;
            doctor.UpdatedByUserId = request.UserId;

            _dbContext.Doctors.Update(doctor);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
