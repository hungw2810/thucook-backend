﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Doctor;

namespace Thucook.Main.ApiAction.DoctorActions
{
    public class CreateHandler : IRequestHandler<ApiActionLocationRequest<DoctorCreateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public CreateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<DoctorCreateInputModel> request, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrEmpty(request.Input.DoctorCode))
            {
                var doctorCodeCheck = await (from d in _dbContext.Doctors
                                             where
                                             d.LocationId == request.LocationId &&
                                             d.IsDeleted == false &&
                                             d.DoctorCode == request.Input.DoctorCode
                                             select d).FirstOrDefaultAsync(cancellationToken);
                if (doctorCodeCheck != null)
                {
                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateDoctorCode);
                }
            }
            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            var maxSequence = await (from p in _dbContext.Doctors
                                     select (long?)p.DoctorSequence)
                                    .MaxAsync(cancellationToken);
            var doctorSequence = maxSequence.HasValue ? maxSequence.Value + 1 : 1;
            var doctorCode = "DR" + doctorSequence.ToString().PadLeft(6, '0');
            var doctorId = Guid.NewGuid();

            var newDoctor = new Doctor
            {
                DoctorId = doctorId,
                LocationId = request.LocationId,
                DoctorName = request.Input.DoctorName,
                DoctorCode = doctorCode,
                DoctorSequence = doctorSequence,
                PhoneNumber = request.Input.PhoneNumber,
                Email = request.Input.Email,
                YearOfExperience = request.Input.YearsOfExperience.HasValue ? request.Input.YearsOfExperience : null,
                SpecialtyId = request.Input.SpecialtyId.HasValue ? request.Input.SpecialtyId : null,
                IsEnabled = true,
                CreatedAt = DateTime.Now,
                CreatedByUserId = request.UserId
            };

            _dbContext.Doctors.Add(newDoctor);

            var newDoctorSettings = new DoctorSetting
            {
                DoctorId = doctorId,
                TimePerAppointmentInMinutes = request.Input.TimePerAppointmentInMinutes,
                BufferTimePerAppointmentInMinutes = request.Input.BufferTimePerAppointmentInMinutes,
                IsVisibleForBooking = true,
                CreatedAt = DateTime.Now,
                CreatedByUserId = request.UserId
            };

            _dbContext.DoctorSettings.Add(newDoctorSettings);

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
