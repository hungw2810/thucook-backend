﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.Commons.Enums;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Doctor;

namespace Thucook.Main.ApiAction.DoctorActions
{
    public class UpdateHandler : IRequestHandler<ApiActionLocationRequest<DoctorUpdateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public UpdateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<DoctorUpdateInputModel> request, CancellationToken cancellationToken)
        {
            var doctor = await (from d in _dbContext.Doctors
                                where
                                d.LocationId == request.LocationId &&
                                d.IsDeleted == false &&
                                d.DoctorId == request.Input.DoctorId
                                select d).FirstOrDefaultAsync(cancellationToken);
            if (doctor == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DoctorNotFound);
            }

            var setting = await (from s in _dbContext.DoctorSettings
                                 where
                                 s.DoctorId == doctor.DoctorId
                                 select s).FirstOrDefaultAsync(cancellationToken);

            // In case of time settings changed then set doctor's appointments as invalid
            if (setting.TimePerAppointmentInMinutes != request.Input.Details.TimePerAppointmentInMinutes ||
                setting.BufferTimePerAppointmentInMinutes != request.Input.Details.BufferTimePerAppointmentInMinutes)
            {
                var appointments = await (from a in _dbContext.Appointments
                                          where
                                          a.DoctorId == request.Input.DoctorId &&
                                          a.LocationId == request.LocationId &&
                                          a.AppointmentStatusId < (short)AppointmentStatusEnum.CheckedIn &&
                                          a.StartDatetime >= DateTime.UtcNow
                                          select a).ToListAsync(cancellationToken);
                appointments.ForEach(a =>
                {
                    a.AppointmentStatusId = (short)AppointmentStatusEnum.Invalid;
                    a.UpdatedAt = DateTime.UtcNow;
                    a.UpdatedByUserId = request.UserId;
                });
                await _dbContext.Appointments.BulkUpdateAsync(appointments);
            }

            doctor.DoctorCode = request.Input.Details.DoctorCode;
            doctor.PhoneNumber = request.Input.Details.PhoneNumber;
            doctor.Email = request.Input.Details.PhoneNumber;
            doctor.YearOfExperience = request.Input.Details.YearsOfExperience.HasValue ? request.Input.Details.YearsOfExperience : null;
            doctor.SpecialtyId = request.Input.Details.SpecialtyId.HasValue ? request.Input.Details.SpecialtyId : null;
            doctor.IsEnabled = request.Input.Details.IsEnabled;
            _dbContext.Doctors.Update(doctor);

            setting.TimePerAppointmentInMinutes = request.Input.Details.TimePerAppointmentInMinutes;
            setting.BufferTimePerAppointmentInMinutes = request.Input.Details.BufferTimePerAppointmentInMinutes;
            setting.IsVisibleForBooking = request.Input.Details.IsVisibleForBooking;
            _dbContext.DoctorSettings.Update(setting);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
