﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Employee;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.EmployeeActions
{
    public class GetPermissionsHandler : IRequestHandler<ApiActionLocationRequest<EmployeeGetPermissionsInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public GetPermissionsHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<EmployeeGetPermissionsInputModel> request, CancellationToken cancellationToken)
        {
            var employee = await (from e in _dbContext.Employees
                                  where
                                  e.UserId == request.UserId &&
                                  e.LocationId == request.LocationId &&
                                  !e.IsDeleted && e.IsEnabled
                                  select e).FirstOrDefaultAsync(cancellationToken);
            if (employee == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.EmployeeNotFound);
            }

            return ApiResponse.CreateModel(new EmployeePermissionsResponseModel
            {
                RoleId = employee.RoleId,
            });
        }
    }
}
