﻿using MediatR;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Employee;
using Thucook.Commons.Utils;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Microsoft.EntityFrameworkCore;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiAction.EmployeeActions
{
    public class InviteUserHandler : IRequestHandler<ApiActionLocationRequest<EmployeeInviteUsersInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public InviteUserHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<EmployeeInviteUsersInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            if (!ValidateInput.IsValidEmail(request.Input.Email))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidEmail);
            }

            var user = await (from u in _dbContext.Users
                              where
                              u.UserName == request.Input.Email
                              select u).FirstOrDefaultAsync(cancellationToken);

            if (user != null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.EmailAlreadyUsed);
            }

            var roleIds = await (from r in _dbContext.Roles
                                 select r.RoleId).ToListAsync(cancellationToken);
            if (!roleIds.Contains((short)request.Input.RoleId))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.RoleNotFound);
            }
            #endregion
            using var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

            var userId = Guid.NewGuid();
            user = new User
            {
                UserId = userId,
                UserName = request.Input.Email,
                PasswordHashed = StringHelper.HashString(request.Input.Password),
                UserTypeId = (int)UserTypeEnum.CrmUser,
                UserStatusId = (int)UserStatusEnum.Normal,
                CreatedByUserId = request.UserId,
                CreatedAt = DateTime.Now,
            };
            _dbContext.Users.Add(user);

            var userInfo = new UserInformation
            {
                UserId = userId,
                Email = request.Input.Email,
                CreatedByUserId = request.UserId,
                CreatedAt = DateTime.Now
            };
            _dbContext.UserInformations.Add(userInfo);

            var employee = new Employee
            {
                UserId = userId,
                LocationId = request.LocationId,
                RoleId = (short)request.Input.RoleId,
                CreatedByUserId = request.UserId,
                CreatedAt = DateTime.Now
            };
            _dbContext.Employees.Add(employee);

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);


            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
