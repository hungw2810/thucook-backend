﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Patient;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.PatientActions
{
    public class SearchHandler : IRequestHandler<ApiActionLocationRequest<PatientSearchInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SearchHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<PatientSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = (from p in _dbContext.Patients
                         where (!request.Input.Details.Gender.HasValue ||
                                p.Gender == (short)request.Input.Details.Gender)
                            && (request.Input.Details.YearOfBirth == 0 ||
                                p.Birthday.Year == request.Input.Details.YearOfBirth) &&
                                !p.IsDeleted
                         select new
                         {
                             p.PatientId,
                             p.FullName,
                             p.PatientCode,
                             p.Birthday,
                             p.Gender,
                             p.PhoneNumber,
                             p.Address
                         })
                         .Distinct();

            // Check if input contains patient code
            if (!string.IsNullOrEmpty(request.Input.Details.PatientCode))
            {
                query = from item in query
                        where
                        EF.Functions.Like(item.PatientCode, $"{request.Input.Details.PatientCode}%")
                        select item;
            }

            if (!string.IsNullOrEmpty(request.Input.Details.Keyword))
            {
                query = from item in query
                        where
                        EF.Functions.Like(item.FullName, $"{request.Input.Details.Keyword}%") ||
                         EF.Functions.Like(item.PhoneNumber, $"{request.Input.Details.Keyword}%")
                        select item;
            }

            // Page info
            var totalItems = await query.CountAsync(cancellationToken);
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);

            var patients = await query
                .Skip(request.Input.PageSize * (request.Input.PageNumber - 1))
                .Take(request.Input.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(patients.Select(p => new PatientResponseModel
            {
                PatientId = p.PatientId,
                FullName = p.FullName,
                PatientCode = p.PatientCode,
                PhoneNumber = p.PhoneNumber,
                Address = p.Address
            }).ToArray(), requestPaging);
        }
    }
}

