﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.Commons.Enums;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Patient;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.PatientActions
{
    public class ReadHandler : IRequestHandler<ApiActionAuthenticatedRequest<PatientReadInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public ReadHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<PatientReadInputModel> request, CancellationToken cancellationToken)
        {
            var patient = await (from p in _dbContext.Patients
                                 where
                                 p.PatientId == request.Input.PatientId &&
                                 !p.IsDeleted
                                 select new PatientResponseModel
                                 {
                                     FullName = p.FullName,
                                     BirthdayUnix = p.Birthday.ToUnixTime(),
                                     Gender = (GenderEnum)p.Gender,
                                     UserId = p.UserId,
                                     IsDefault = p.IsDefault,
                                     PhoneNumber = p.PhoneNumber,
                                     Address = p.Address,
                                     Email = p.Email,
                                     HeightInCm = p.HeightInCm,
                                     WeightInKg = p.WeightInKg,
                                     Allergy = p.Allergy
                                 }).FirstOrDefaultAsync(cancellationToken);

            if (patient == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.PatientNotFound);
            }

            return ApiResponse.CreateModel(patient);
        }
    }
}

