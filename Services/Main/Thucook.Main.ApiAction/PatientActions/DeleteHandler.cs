﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Patient;

namespace Thucook.Main.ApiAction.PatientActions
{
    public class DeleteHandler : IRequestHandler<ApiActionAuthenticatedRequest<PatientDeleteInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public DeleteHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<PatientDeleteInputModel> request, CancellationToken cancellationToken)
        {
            var patient = await (from p in _dbContext.Patients
                                 where
                                 p.PatientId == request.Input.PatientId &&
                                 !p.IsDeleted
                                 select p)
                                 .FirstOrDefaultAsync(cancellationToken);
            if (patient == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.PatientNotFound);
            }

            patient.IsDeleted = true;
            patient.UpdatedByUserId = request.UserId;
            patient.UpdatedAt = DateTime.Now;

            _dbContext.Patients.Update(patient);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}

