﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.Commons.Enums;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Patient;

namespace Thucook.Main.ApiAction.PatientActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<PatientCreateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public CreateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<PatientCreateInputModel> request, CancellationToken cancellationToken)
        {
            // Check who create patient
            // If location -> userId = null
            // If end-user -> userId = request.UserId
            #region Validate input
            var nameCheck = await (from p in _dbContext.Patients
                                   where
                                   p.FullName == request.Input.FullName &&
                                   !p.IsDeleted
                                   select p.PatientId)
                                   .FirstOrDefaultAsync(cancellationToken);

            if (nameCheck != Guid.Empty)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.OK, ApiInternalErrorMessages.DuplicatePatientFullName);
            }

            var userType = await (from u in _dbContext.Users
                                  where
                                  u.UserId == request.UserId &&
                                  u.UserStatusId == (int)UserStatusEnum.Normal
                                  select u.UserTypeId)
                                  .FirstOrDefaultAsync(cancellationToken);
            #endregion
            using var transaction = await _dbContext.Database.BeginTransactionAsync();

            var maxSequence = await (from p in _dbContext.Patients
                                     select (long?)p.PatientSequence)
                                     .MaxAsync(cancellationToken);
            var patientSequence = maxSequence.HasValue ? maxSequence.Value + 1 : 1;
            var patientCode = "PA" + patientSequence.ToString().PadLeft(6, '0');

            var patient = new Patient
            {
                UserId = userType == (short)UserTypeEnum.EndUser ? request.UserId : null,
                PatientCode = patientCode,
                PatientSequence = patientSequence,
                FullName = request.Input.FullName,
                Birthday = request.Input.BirthdayUnixTime.ToDateTimeFromUnixTime(),
                Gender = (short)request.Input.Gender,
                PhoneNumber = request.Input.PhoneNumber,
                Address = request.Input.Address,
                Email = request.Input.Email,
                HeightInCm = request.Input.HeightInCm ?? null,
                WeightInKg = request.Input.WeightInKg ?? null,
                Allergy = !string.IsNullOrEmpty(request.Input.Allergy) ? request.Input.Allergy : null,
                CreatedByUserId = request.UserId,
                CreatedAt = DateTime.Now
            };
            _dbContext.Patients.Add(patient);

            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}

