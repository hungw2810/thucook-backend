﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Patient;

namespace Thucook.Main.ApiAction.PatientActions
{
    public class UpdateHandler : IRequestHandler<ApiActionAuthenticatedRequest<PatientUpdateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public UpdateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<PatientUpdateInputModel> request, CancellationToken cancellationToken)
        {
            var patient = await (from p in _dbContext.Patients
                                 where
                                 p.PatientId == request.Input.PatientId &&
                                 !p.IsDeleted
                                 select p).FirstOrDefaultAsync(cancellationToken);

            if (patient == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.PatientNotFound);
            }

            patient.FullName = request.Input.Details.FullName;
            patient.Birthday = request.Input.Details.BirthdayUnixTime.ToDateTimeFromUnixTime();
            patient.Gender = (short)request.Input.Details.Gender;
            patient.PhoneNumber = request.Input.Details.PhoneNumber;
            patient.Address = request.Input.Details.Address;
            patient.Email = request.Input.Details.Email;
            patient.HeightInCm = request.Input.Details.HeightInCm ?? null;
            patient.Allergy = request.Input.Details.Allergy;

            _dbContext.Patients.Update(patient);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}

