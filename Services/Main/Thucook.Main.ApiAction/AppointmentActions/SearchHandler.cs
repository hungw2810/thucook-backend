﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.Commons.Enums;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Appointment;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.AppointmentActions
{
    public class SearchHandler : IRequestHandler<ApiActionLocationRequest<AppointmentSearchInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SearchHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<AppointmentSearchInputModel> request, CancellationToken cancellationToken)
        {
            var start = request.Input.Details.FromDate.ToDateTimeFromUnixTime();
            var end = request.Input.Details.ToDate.ToDateTimeFromUnixTime();

            var query = (from a in _dbContext.Appointments
                         join d in _dbContext.Doctors on a.DoctorId equals d.DoctorId
                         where
                         (request.Input.Details.DoctorId == null || a.DoctorId == request.Input.Details.DoctorId) &&
                         (request.Input.Details.SpecialtyId == null || d.SpecialtyId == request.Input.Details.SpecialtyId) &&
                         (request.Input.Details.AppointmentStatusId == null || (AppointmentStatusEnum)a.AppointmentStatusId == request.Input.Details.AppointmentStatusId) &&
                         (a.StartDatetime <= end && a.StartDatetime >= start)
                         select new
                         {
                             a.AppointmentId,
                             a.Patient.PatientId,
                             a.Patient.FullName,
                             a.Patient.PhoneNumber,
                             a.Patient.Email,
                             a.Patient.Birthday,
                             a.Patient.Gender,
                             d.DoctorName,
                             a.Symptom,
                             a.StartDatetime,
                             a.AppointmentStatusId,
                             a.CreatedAt
                         }).Distinct();
            // Filter by keyword
            if (!string.IsNullOrEmpty(request.Input.Details.Keyword) && request.Input.Details.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Like(new string[] { item.FullName, item.PhoneNumber, item.Email }, $"{request.Input.Details.Keyword}%")
                    select item;
            }
            // Page info
            var totalItems = query.Count();
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);
            // Order
            query = query
                .OrderBy(i => i.StartDatetime);
            // Page data
            var appointments = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(appointments.Select(a => new AppointmentReponseModel
            {
                AppointmentId = a.AppointmentId,
                Patient = new PatientResponseModel
                {
                    FullName = a.FullName,
                    BirthdayUnix = a.Birthday.ToUnixTime(),
                    Gender = (GenderEnum)a.Gender,
                },
                DoctorName = a.DoctorName,
                AppointmentStatusId = (AppointmentStatusEnum)a.AppointmentStatusId,
                StartDatetimeUnix = a.StartDatetime.ToUnixTime()

            })
               .ToArray(), requestPaging);
        }
    }
}

