﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.Commons.Enums;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Appointment;

namespace Thucook.Main.ApiAction.AppointmentActions
{
    public class CheckinHandler : IRequestHandler<ApiActionLocationRequest<AppointmentCheckinInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public CheckinHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<AppointmentCheckinInputModel> request, CancellationToken cancellationToken)
        {
            var appointment = await (from a in _dbContext.Appointments
                                     where
                                     a.LocationId == request.LocationId &&
                                     a.AppointmentId == request.Input.AppointmentId
                                     select a).FirstOrDefaultAsync(cancellationToken);

            if (appointment == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.AppointmentNotFound);
            }

            if (appointment.AppointmentStatusId >= (short)AppointmentStatusEnum.CheckedIn)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.AppointmentStatusNotAllow);
            }

            appointment.ActualCheckInDatetime = DateTime.Now;
            appointment.AppointmentStatusId = (short)AppointmentStatusEnum.CheckedIn;
            _dbContext.Appointments.Update(appointment);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
