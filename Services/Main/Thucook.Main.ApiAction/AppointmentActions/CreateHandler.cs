﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.Commons.Constants;
using Thucook.Commons.Enums;
using Thucook.Commons.Extensions;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Appointment;
using Thucook.Main.ApiService;

namespace Thucook.Main.ApiAction.AppointmentActions
{
    public class CreateHandler : IRequestHandler<ApiActionAuthenticatedRequest<AppointmentCreateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;
        private readonly IDoctorScheduleService _doctorScheduleService;

        public CreateHandler(ThucookContext dbContext, IDoctorScheduleService doctorScheduleService)
        {
            _dbContext = dbContext;
            _doctorScheduleService = doctorScheduleService;
        }

        public async Task<IApiResponse> Handle(ApiActionAuthenticatedRequest<AppointmentCreateInputModel> request, CancellationToken cancellationToken)
        {
            #region Validate input
            var patient = await (from p in _dbContext.Patients
                                 where
                                 p.UserId == request.UserId &&
                                 p.PatientId == request.Input.PatientId &&
                                 p.IsDeleted == false
                                 select p).FirstOrDefaultAsync(cancellationToken);
            if (patient == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadGateway, ApiInternalErrorMessages.PatientNotFound);
            }

            var location = await (from l in _dbContext.Locations
                                  where
                                  l.LocationId == request.Input.LocationId &&
                                  l.IsDeleted == false &&
                                  l.IsEnabled == true
                                  select l).FirstOrDefaultAsync(cancellationToken);
            if (location == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadGateway, ApiInternalErrorMessages.LocationNotFound);
            }

            var doctor = await (from d in _dbContext.Doctors
                                join ds in _dbContext.DoctorSettings on d.DoctorId equals ds.DoctorId
                                where
                                d.DoctorId == request.Input.DoctorId &&
                                d.IsDeleted == false &&
                                d.IsEnabled == true &&
                                ds.IsVisibleForBooking == true
                                select new
                                { 
                                    d.DoctorId,
                                    ds.BufferTimePerAppointmentInMinutes,
                                    ds.TimePerAppointmentInMinutes
                                }).FirstOrDefaultAsync(cancellationToken);
            if (doctor == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DoctorNotFound);
            }

            if (DateTime.Now.ToUnixTime() > (request.Input.StartDateTimeUnix - AppointmentConstants.AllowBookingBeforeMinutes * 60000))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidAppointmentTime);
            }

            var startTime = request.Input.StartDateTimeUnix.ToDateTimeFromUnixTime();
            var endTime = startTime.AddMinutes(doctor.TimePerAppointmentInMinutes);
            var startOfDay = startTime.Date;
            var endOfDay = startOfDay.AddHours(23).AddMinutes(59).AddSeconds(59);

            var apps = await (from a in _dbContext.Appointments
                              where
                              a.PatientId == request.Input.PatientId &&
                              a.AppointmentStatusId < (short)AppointmentStatusEnum.Finished &&
                              a.StartDatetime >= startOfDay &&
                              a.StartDatetime <= endOfDay
                              select a)
                             .ToArrayAsync(cancellationToken);
            if (apps.Where(a => a.LocationId == request.Input.LocationId).Any())
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.TooMuchAppointmentsOnSameDay);
            }

            if (apps.Where(a => a.StartDatetime <= endTime && a.EndDatetime >= startTime).Any())
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.TooMuchAppointmentsAtSameTime);
            }
            #endregion

            #region Check appointment in an available slot
            using var transaction = _dbContext.Database.BeginTransaction();

            var schedule = await (from d in _dbContext.DoctorSchedules.AsNoTracking()
                                  where
                                  d.ScheduleId == request.Input.ScheduleId &&
                                  d.DoctorId == request.Input.DoctorId
                                  select d)
                                   .FirstOrDefaultAsync(cancellationToken);
            if (schedule == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.ScheduleNotFound);
            }

            var scheduleSlots = await _doctorScheduleService.GetDoctorAvailableTimeSlots(request.Input.LocationId, request.Input.DoctorId, cancellationToken);

            var isValid = false;
            foreach (var occ in scheduleSlots)
            {
                foreach (long slot in occ.Slots)
                {
                    if (request.Input.StartDateTimeUnix == slot)
                    {
                        isValid = true;
                        break;
                    }
                }
                if (isValid)
                {
                    break;
                }
            }
            if (!isValid)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.InvalidAppointmentTime);
            }
            #endregion

            // Create new appointment
            var appointment = new Appointment()
            {
                PatientId = request.Input.PatientId,
                LocationId = request.Input.LocationId,
                DoctorId = request.Input.DoctorId,
                ScheduleId = request.Input.ScheduleId,
                StartDatetime = request.Input.StartDateTimeUnix.ToDateTimeFromUnixTime(),
                EndDatetime = request.Input.StartDateTimeUnix.ToDateTimeFromUnixTime().AddMinutes(doctor.TimePerAppointmentInMinutes),
                AppointmentStatusId = (short)AppointmentStatusEnum.NotApproved,
                Symptom = request.Input.Symptom,
                CreatedByUserId = request.UserId,
                CreatedAt = DateTime.UtcNow
            };

            _dbContext.Appointments.Add(appointment);
            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
