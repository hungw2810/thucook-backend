﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Location;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.LocationActions
{
    public class GetInfomationsHandler : IRequestHandler<ApiActionLocationRequest<LocationGetInformationsInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public GetInfomationsHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<LocationGetInformationsInputModel> request, CancellationToken cancellationToken)
        {
            var location = await (from l in _dbContext.Locations
                                  where
                                  l.LocationId == request.LocationId &&
                                  !l.IsDeleted
                                  select new
                                  {
                                      l.LocationId,
                                      l.LocationName,
                                      Specialities = l.Specialties
                                            .Where(s => !s.IsDeleted && s.IsEnabled)
                                            .Select(s => new LocationInfoSpecialtyResponseModel
                                            {
                                                SpecialtyId = s.SpecialtyId,
                                                SpecialtyName = s.SpecialtyName,
                                            }).ToArray(),
                                      Doctors = l.Doctors
                                           .Where(d => !d.IsDeleted && d.IsEnabled)
                                           .Select(d => new LocationInfoDoctorResponseModel
                                           {
                                               DoctorId = d.DoctorId,
                                               //DoctorName = d.DoctorName,
                                               SpecialtyId = d.SpecialtyId
                                           }).ToArray(),
                                      Symptoms = l.Symptoms
                                           .Where(s => !s.IsDeleted && s.IsEnabled)
                                           .Select(s => new LocationInfoSymptomResponseModel
                                           {
                                               SymptomId = s.SymptomId,
                                               SymptomName = s.SymptomName,
                                               Content = s.Content,
                                               SpecialtyId = s.SpecialtyId
                                           }).ToArray()
                                  }).FirstOrDefaultAsync(cancellationToken);

            if (location == null)
            {
                return ApiResponse.CreateErrorModel(System.Net.HttpStatusCode.BadRequest, ApiInternalErrorMessages.LocationNotFound);
            }

            return ApiResponse.CreateModel(new LocationInformationsResponseModel
            {
                LocationId = location.LocationId,
                LocationName = location.LocationName,
                Specialties = location.Specialities,
                Doctors = location.Doctors,
                Symptoms = location.Symptoms
            });
        }
    }
}
