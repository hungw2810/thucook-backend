﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Symptom;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.SymptomActions
{
    public class SearchHandler : IRequestHandler<ApiActionLocationRequest<SymptomSearchInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SearchHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SymptomSearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = (from s in _dbContext.Symptoms
                         where
                         s.LocationId == request.LocationId &&
                         !s.IsDeleted
                         select new
                         {
                             s.SymptomId,
                             s.SymptomName,
                             s.IsEnabled,
                             s.Content,
                             s.Specialty.SpecialtyName,
                         })
                         .Distinct();
            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Like(item.SymptomName, $"{request.Input.Keyword}%")
                    select item;
            }
            // Page info
            var totalItems = await query.CountAsync(cancellationToken);
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);
            // Order
            query = query
                .OrderBy(u => u.SymptomName);
            // Page data
            var specialties = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                specialties.Select(s => new SymptomResponseModel
                {
                    SymptomId = s.SymptomId,
                    SpecialtyName = s.SpecialtyName,
                    SymptomName = s.SymptomName,
                    Content = s.Content,
                    IsEnabled = s.IsEnabled,
                }).ToList(),
                requestPaging);
        }
    }
}
