﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Symptom;

namespace Thucook.Main.ApiAction.SymptomActions
{
    public class DeleteHandler : IRequestHandler<ApiActionLocationRequest<SymptomDeleteInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public DeleteHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SymptomDeleteInputModel> request, CancellationToken cancellationToken)
        {
            var symptom = await (from s in _dbContext.Symptoms
                                 where
                                 s.LocationId == request.LocationId &&
                                 s.IsDeleted == false &&
                                 s.SymptomId == request.Input.SymptomId
                                 select s).FirstOrDefaultAsync(cancellationToken);
            if (symptom == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SymptomNotFound);
            }

            symptom.IsDeleted = true;
            symptom.UpdatedByUserId = request.UserId;
            symptom.UpdatedAt = DateTime.Now;

            _dbContext.Symptoms.Update(symptom);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
