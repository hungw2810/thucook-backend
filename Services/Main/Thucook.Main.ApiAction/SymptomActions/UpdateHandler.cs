﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Symptom;

namespace Thucook.Main.ApiAction.SymptomActions
{
    public class UpdateHandler : IRequestHandler<ApiActionLocationRequest<SymptomUpdateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public UpdateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SymptomUpdateInputModel> request, CancellationToken cancellationToken)
        {
            var symptom = await (from s in _dbContext.Symptoms
                                 where
                                 s.LocationId == request.LocationId &&
                                 s.SymptomId == request.Input.SymptomId &&
                                 !s.IsDeleted
                                 select s).FirstOrDefaultAsync(cancellationToken);
            if (symptom == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SymptomNotFound);
            }

            var nameCheck = await (from s in _dbContext.Symptoms
                                   where
                                   s.LocationId == request.LocationId &&
                                   s.SymptomId != request.Input.SymptomId &&
                                   s.SymptomName == request.Input.Details.SymptomName &&
                                   !s.IsDeleted
                                   select s.SymptomName).FirstOrDefaultAsync(cancellationToken);
            if (!string.IsNullOrEmpty(nameCheck))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateName);
            }

            symptom.SpecialtyId = request.Input.Details.SpecialtyId.Value;
            symptom.SymptomName = request.Input.Details.SymptomName;
            symptom.Content = request.Input.Details.Content;
            symptom.UpdatedByUserId = request.UserId;
            symptom.UpdatedAt = DateTime.Now;

            _dbContext.Symptoms.Update(symptom);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
