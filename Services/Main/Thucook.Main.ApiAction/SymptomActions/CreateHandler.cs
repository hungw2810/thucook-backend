﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Symptom;

namespace Thucook.Main.ApiAction.SymptomActions
{
    public class CreateHandler : IRequestHandler<ApiActionLocationRequest<SymptomCreateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;
        public CreateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SymptomCreateInputModel> request, CancellationToken cancellationToken)
        {
            // Validate input
            var query = await (from s in _dbContext.Symptoms
                               where
                               s.LocationId == request.LocationId &&
                               s.SymptomName == request.Input.SymptomName &&
                               !s.IsDeleted
                               select s.SymptomName).FirstOrDefaultAsync(cancellationToken);
            if (!string.IsNullOrEmpty(query))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateName);
            }

            var newSymptom = new Symptom
            {
                SymptomId = Guid.NewGuid(),
                LocationId = request.LocationId,
                SpecialtyId = request.Input.SpecialtyId,
                SymptomName = request.Input.SymptomName,
                Content = request.Input.Content,
                IsEnabled = true,
                CreatedByUserId = request.UserId,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
            };

            _dbContext.Symptoms.Add(newSymptom);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
