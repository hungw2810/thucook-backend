﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Symptom;

namespace Thucook.Main.ApiAction.SymptomActions
{
    internal class SetEnabledHandler : IRequestHandler<ApiActionLocationRequest<SymptomSetEnabledInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SetEnabledHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SymptomSetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var symptom = await (from s in _dbContext.Symptoms
                                 where
                                 s.LocationId == request.LocationId &&
                                 s.SymptomId == request.Input.SymptomId &&
                                 !s.IsDeleted
                                 select s).FirstOrDefaultAsync(cancellationToken);
            if (symptom == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SymptomNotFound);
            }

            symptom.IsEnabled = request.Input.IsEnabled;
            symptom.UpdatedByUserId = request.UserId;
            symptom.UpdatedAt = DateTime.Now;
            _dbContext.Update(symptom);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
