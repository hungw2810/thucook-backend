﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Symptom;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.SymptomActions
{
    public class ReadHandler : IRequestHandler<ApiActionLocationRequest<SymptomReadInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public ReadHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SymptomReadInputModel> request, CancellationToken cancellationToken)
        {
            var symptom = await (from s in _dbContext.Symptoms
                                 where
                                 s.LocationId == request.LocationId &&
                                 s.IsDeleted == false &&
                                 s.SymptomId == request.Input.SymptomId
                                 select s).FirstOrDefaultAsync(cancellationToken);
            if (symptom == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SymptomNotFound);
            }

            return ApiResponse.CreateModel(new SymptomResponseModel
            {
                SymptomId = request.Input.SymptomId,
                SpecialtyId = symptom.SpecialtyId,
                SymptomName = symptom.SymptomName,
                Content = symptom.Content,
                IsEnabled = symptom.IsEnabled,
            });
        }
    }
}
