﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Specialty;

namespace Thucook.Main.ApiAction.SpecialtyActions
{
    public class SetEnabledHandler : IRequestHandler<ApiActionLocationRequest<SpecialtySetEnabledInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SetEnabledHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SpecialtySetEnabledInputModel> request, CancellationToken cancellationToken)
        {
            var specialty = await (from s in _dbContext.Specialties
                                   where
                                   s.LocationId == request.LocationId &&
                                   s.IsDeleted == false &&
                                   s.SpecialtyId == request.Input.SpecialtyId
                                   select s).FirstOrDefaultAsync(cancellationToken);
            if (specialty == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SpecialtyNotFound);
            }

            specialty.IsEnabled = request.Input.IsEnabled;
            specialty.UpdatedByUserId = request.UserId;
            specialty.UpdatedAt = DateTime.Now;
            _dbContext.Update(specialty);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
