﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Specialty;

namespace Thucook.Main.ApiAction.SpecialtyActions
{
    public class UpdateHandler : IRequestHandler<ApiActionLocationRequest<SpecialtyUpdateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public UpdateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SpecialtyUpdateInputModel> request, CancellationToken cancellationToken)
        {
            var specialty = await (from s in _dbContext.Specialties
                                   where
                                   s.LocationId == request.LocationId &&
                                   s.IsDeleted == false &&
                                   s.SpecialtyId == request.Input.SpecialtyId
                                   select s).FirstOrDefaultAsync(cancellationToken);
            if (specialty == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SpecialtyNotFound);
            }

            // Validate input
            var nameCheck = await (from s in _dbContext.Specialties
                                   where
                                   s.LocationId == request.LocationId &&
                                   s.IsDeleted == false &&
                                   s.SpecialtyId != request.Input.SpecialtyId &&
                                   s.SpecialtyName == request.Input.Details.SpecialtyName
                                   select s.SpecialtyName).FirstOrDefaultAsync(cancellationToken);
            if (!string.IsNullOrEmpty(nameCheck))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateName);
            }

            if (!string.IsNullOrEmpty(request.Input.Details.SpecialtyShortName))
            {
                var shortNameCheck = await (from s in _dbContext.Specialties
                                            where
                                            s.LocationId == request.LocationId &&
                                            s.SpecialtyId != request.Input.SpecialtyId &&
                                            s.SpecialtyShortName == request.Input.Details.SpecialtyShortName &&
                                            s.IsDeleted == false
                                            select s.SpecialtyName)
                                            .FirstOrDefaultAsync(cancellationToken);
                if (!string.IsNullOrEmpty(shortNameCheck))
                {
                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateShortName);
                }
            }

            specialty.SpecialtyName = request.Input.Details.SpecialtyName;
            specialty.SpecialtyShortName = request.Input.Details.SpecialtyShortName;
            specialty.UpdatedAt = System.DateTime.UtcNow;
            specialty.UpdatedByUserId = request.UserId;

            _dbContext.Update(specialty);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
