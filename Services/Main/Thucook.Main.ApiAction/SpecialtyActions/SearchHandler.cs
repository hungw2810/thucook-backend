﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Specialty;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.SpecialtyActions
{
    public class SearchHandler : IRequestHandler<ApiActionLocationRequest<SpecialtySearchInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public SearchHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SpecialtySearchInputModel> request, CancellationToken cancellationToken)
        {
            var query = (from s in _dbContext.Specialties
                         where
                         s.LocationId == request.LocationId &&
                         s.IsDeleted == false
                         select s)
                         .Distinct();
            // Filter
            if (!string.IsNullOrEmpty(request.Input.Keyword) && request.Input.Keyword.Length >= 2)
            {
                query =
                    from item in query
                    where EF.Functions.Like(item.SpecialtyName, $"{request.Input.Keyword}%")
                    select item;
            }
            // Page info
            var totalItems = await query.CountAsync(cancellationToken);
            var requestPaging = new ApiResponsePaging(request.Input.PageSize, request.Input.PageNumber, totalItems);
            // Order
            query = query
                .OrderBy(u => u.SpecialtyName);
            // Page data
            var specialties = await query
                .Skip(requestPaging.PageSize * (requestPaging.PageNumber - 1))
                .Take(requestPaging.PageSize)
                .ToListAsync(cancellationToken);

            return ApiResponse.CreatePagingModel(
                specialties.Select(s => new SpecialtyResponseModel
                {
                    SpecialtyId = s.SpecialtyId,
                    SpecialtyName = s.SpecialtyName,
                    SpecialtyShortName = s.SpecialtyShortName,
                    IsEnabled = s.IsEnabled,
                }).ToList(),
                requestPaging);
        }
    }
}
