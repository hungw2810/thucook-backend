﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Specialty;

namespace Thucook.Main.ApiAction.SpecialtyActions
{
    public class DeleteHandler : IRequestHandler<ApiActionLocationRequest<SpecialtyDeleteInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public DeleteHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SpecialtyDeleteInputModel> request, CancellationToken cancellationToken)
        {
            var specialty = await (from s in _dbContext.Specialties
                                   where
                                   s.LocationId == request.LocationId &&
                                   s.IsDeleted == false &&
                                   s.SpecialtyId == request.Input.SpecialtyId
                                   select s).FirstOrDefaultAsync(cancellationToken);
            if (specialty == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SpecialtyNotFound);
            }

            specialty.IsDeleted = true;
            _dbContext.Update(specialty);

            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
