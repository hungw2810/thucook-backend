﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Specialty;

namespace Thucook.Main.ApiAction.SpecialtyActions
{
    public class CreateHandler : IRequestHandler<ApiActionLocationRequest<SpecialtyCreateInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public CreateHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SpecialtyCreateInputModel> request, CancellationToken cancellationToken)
        {
            // Validate input
            var nameCheck = await (from s in _dbContext.Specialties
                                   where
                                   s.LocationId == request.LocationId &&
                                   s.IsDeleted == false &&
                                   s.SpecialtyName == request.Input.SpecialtyName
                                   select s.SpecialtyName).FirstOrDefaultAsync(cancellationToken);
            if (!string.IsNullOrEmpty(nameCheck))
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateName);
            }

            if (!string.IsNullOrEmpty(request.Input.SpecialtyShortName))
            {
                var shortNameCheck = await (from s in _dbContext.Specialties
                                            where
                                            s.LocationId == request.LocationId &&
                                            s.IsDeleted == false &&
                                            s.SpecialtyShortName == request.Input.SpecialtyShortName
                                            select s.SpecialtyName)
                                            .FirstOrDefaultAsync(cancellationToken);
                if (!string.IsNullOrEmpty(shortNameCheck))
                {
                    return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.DuplicateShortName);
                }
            }

            var specialty = new Specialty
            {
                SpecialtyId = Guid.NewGuid(),
                LocationId = request.LocationId,
                SpecialtyName = request.Input.SpecialtyName,
                SpecialtyShortName = request.Input.SpecialtyShortName,
                CreatedAt = DateTime.Now,
                CreatedByUserId = request.UserId,
            };
            _dbContext.Specialties.Add(specialty);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return ApiResponse.CreateModel(HttpStatusCode.OK);
        }
    }
}
