﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Thucook.EntityFramework;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiErrorMessages;
using Thucook.Main.ApiModel.ApiInputModels.Specialty;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.ApiAction.SpecialtyActions
{
    public class ReadHandler : IRequestHandler<ApiActionLocationRequest<SpecialtyReadInputModel>, IApiResponse>
    {
        private readonly ThucookContext _dbContext;

        public ReadHandler(ThucookContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IApiResponse> Handle(ApiActionLocationRequest<SpecialtyReadInputModel> request, CancellationToken cancellationToken)
        {
            var specialty = await (from s in _dbContext.Specialties
                                   where
                                   s.LocationId == request.LocationId &&
                                   s.IsDeleted == false &&
                                   s.SpecialtyId == request.Input.SpecialtyId
                                   select s).FirstOrDefaultAsync(cancellationToken);
            if (specialty == null)
            {
                return ApiResponse.CreateErrorModel(HttpStatusCode.BadRequest, ApiInternalErrorMessages.SpecialtyNotFound);
            }
            return ApiResponse.CreateModel(new SpecialtyResponseModel
            {
                SpecialtyId = specialty.SpecialtyId,
                SpecialtyName = specialty.SpecialtyName,
                SpecialtyShortName = specialty.SpecialtyShortName,
                IsEnabled = specialty.IsEnabled
            });
        }
    }
}
