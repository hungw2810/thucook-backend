﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Schedule;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("schedules")]
    [ApiController]
    public class ScheduleController : BaseController
    {
        public ScheduleController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new schedule
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] ScheduleCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, input));
        }

        /// <summary>
        /// Get details of doctor
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{scheduleId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<ScheduleDetailsResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Read([FromRoute] long scheduleId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new ScheduleReadInputModel
            {
                ScheduleId = scheduleId
            }));
        }

        /// <summary>
        /// Update schedule
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{scheduleId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long scheduleId, [FromBody] ScheduleUpdateInput input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new ScheduleUpdateInputModel
            {
                ScheduleId = scheduleId,
                Details = input
            }));
        }

        /// <summary>
        /// Delete schedule
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{scheduleId}/delete")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long scheduleId, [FromBody] ScheduleDeleteInput input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new ScheduleDeleteInputModel
            {
                ScheduleId = scheduleId,
                Details = input
            }));
        }


    }
}
