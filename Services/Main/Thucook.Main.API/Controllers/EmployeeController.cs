﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Employee;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("employees")]
    [ApiController]
    public class EmployeeController : BaseController
    {
        public EmployeeController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Invite users to location
        /// (Location-Only)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("invite-users")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> InviteUsers([FromBody] EmployeeInviteUsersInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, input));
        }

        /// <summary>
        /// Get my permissions
        /// (Location-Only)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("my-permissions")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<EmployeePermissionsResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetMyPermissions()
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new EmployeeGetPermissionsInputModel()));
        }
    }
}
