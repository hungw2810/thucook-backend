﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Doctor;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("doctors")]
    [ApiController]
    public class DoctorController : BaseController
    {
        public DoctorController(IMediator mediator
            , ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new doctor
        /// (Location-Only)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] DoctorCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, input));
        }

        /// <summary>
        /// Get details of doctor
        /// (Location-Only)
        /// <param name="doctorId"></param> 
        /// <returns></returns>
        [HttpGet]
        [Route("{doctorId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<DoctorResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Read([FromRoute] Guid doctorId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new DoctorReadInputModel
            {
                DoctorId = doctorId
            }));
        }

        /// <summary>
        /// Update doctor
        /// (Location-Only)
        /// </summary>
        /// <param name="doctorId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{doctorId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] Guid doctorId, [FromBody] DoctorUpdateModel details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new DoctorUpdateInputModel
            {
                DoctorId = doctorId,
                Details = details
            }));
        }

        /// <summary>
        /// Delete doctor
        /// (Location-Only)
        /// </summary>
        /// <param name="doctorId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{doctorId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] Guid doctorId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new DoctorDeleteInputModel
            {
                DoctorId = doctorId
            }));
        }

        /// <summary>
        /// Set enabled doctor
        /// (Location-Only)
        /// </summary>
        /// <param name="doctorId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{doctorId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] Guid doctorId, [FromBody] DoctorSetEnabledInput input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new DoctorSetEnabledInputModel
            {
                DoctorId = doctorId,
                IsEnabled = input.IsEnabled
            }));
        }

        /// <summary>
        /// Search all doctor of location by pages
        /// (Location-Only)
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<DoctorResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [Range(1, 100)] int pageSize = 50,
            [Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new DoctorSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }
    }
}
