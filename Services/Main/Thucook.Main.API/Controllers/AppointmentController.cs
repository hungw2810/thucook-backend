﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Appointment;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("appointments")]
    [ApiController]
    public class AppointmentController : BaseController
    {
        public AppointmentController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Search appointment for clinic
        /// (Location-Only)
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("search")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<AppointmentReponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromBody] AppointmentSearchInput input,
            [Range(1, 100)] int pageSize = 50,
            [Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new AppointmentSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Details = input
            }));
        }

        /// <summary>
        /// Create new appointment
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] AppointmentCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Get details of appointment
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <return></return>
        [HttpGet]
        [Route("{appointmentId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<AppointmentDetailResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromRoute] long appointmentId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new AppointmentGetDetailsInputModel
            {
                AppointmentId = appointmentId
            }));
        }

        /// <summary>
        /// Approve appointment
        /// (Location-Only)
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{appointmentId}/approve")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Approve([FromRoute] long appointmentId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new AppointmentApproveInputModel
            {
                AppointmentId = appointmentId
            }));
        }

        /// <summary>
        /// Check-in appointment
        /// (Location-Only)
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{appointmentId}/check-in")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> CheckIn([FromRoute] long appointmentId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new AppointmentCheckinInputModel
            {
                AppointmentId = appointmentId
            }));
        }
    }
}
