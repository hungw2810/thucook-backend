﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Patient;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("patients")]
    [ApiController]
    public class PatientController : BaseController
    {
        public PatientController(IMediator mediator,
            ICurrentContext currentContext)
        : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new patient
        /// (Auth)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] PatientCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, input));
        }

        /// <summary>
        /// Get patient details
        /// (Auth)
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{patientId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<PatientResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Read([FromRoute] Guid patientId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new PatientReadInputModel
            {
                PatientId = patientId
            }));
        }

        /// <summary>
        /// Search patients by pages
        /// (Location-Only)
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pageSize">Page size</param>
        /// <param name="pageNumber">Page number</param>
        /// <returns></returns>
        [HttpPost]
        [Route("search")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<PatientResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [FromBody] PatientsSearchInput input,
            [Range(1, 100)] int pageSize = 50,
            [Range(1, int.MaxValue)] int pageNumber = 1)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new PatientSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Details = input
            }));
        }

        /// <summary>
        /// Delete patient
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{patientId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] Guid patientId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new PatientDeleteInputModel
            {
                PatientId = patientId
            }));
        }

        /// <summary>
        /// Update patient
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{patientId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] Guid patientId, [FromBody] PatientUpdateModel details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new PatientUpdateInputModel
            {
                PatientId = patientId,
                Details = details
            }));
        }
    }
}

