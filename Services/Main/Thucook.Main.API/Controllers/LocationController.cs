﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Location;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("locations")]
    [ApiController]
    public class LocationController : BaseController
    {
        public LocationController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Get location informations with specialities and doctors
        /// (Location-Only)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("info")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<LocationInformationsResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetInfomation()
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new LocationGetInformationsInputModel()));
        }

        /// <summary>
        /// Get location information for booking
        /// (Patient-Only)
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{locationId}/info-for-booking")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<LocationInformationsResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetInfoForBooking([FromRoute] Guid locationId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new LocationGetInfoForBookingInputModel
            {
                LocationId = locationId
            }));
        }
    }
}
