﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Medicine;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("medicines")]
    [ApiController]
    public class MedicineController : BaseController
    {
        public MedicineController(IMediator mediator
            , ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Add new medicine to library
        /// (Location-Only)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] MedicineCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, input));
        }

        /// <summary>
        /// Search medicine
        /// (Location-Only)
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<MedicineResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [Range(1, 100)] int pageSize = 50,
            [Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new MedicineSearchInputModel
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }

        /// <summary>
        /// Delete medicine
        /// (Location-Only)
        /// </summary>
        /// <param name="medicineId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{medicineId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] long medicineId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new MedicineDeleteInputModel
            {
                MedicineId = medicineId
            }));
        }

        /// <summary>
        /// Update medicine
        /// (Location-Only)
        /// </summary>
        /// <param name="medicineId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{medicineId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] long medicineId, [FromBody] MedicineUpdateInput details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new MedicineUpdateInputModel
            {
                MedicineId = medicineId,
                Details = details
            }));
        }

        /// <summary>
        /// Get details of medicine
        /// (Location-Only)
        /// </summary>
        /// <param name="=medicineId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{medicineId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<MedicineResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDetails([FromRoute] MedicineReadInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, input));
        }
    }
}
