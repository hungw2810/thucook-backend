﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.User;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("users")]
    [ApiController]
    public class UserController : BaseController
    {
        public UserController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Get user infomations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("me")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<UserInfoResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetInfo()
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new UserGetInfoInputModel()));
        }

        /// <summary>
        /// Update information
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromBody] UserUpdateInfoModel details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, new UserUpdateInfoInputModel
            {
                UserId = UserId,
                Details = details
            }));
        }
    }
}
