﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Symptom;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("symptoms")]
    [ApiController]
    public class SymptomController : BaseController
    {
        public SymptomController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new symptom
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] SymptomCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, input));
        }

        /// <summary>
        /// Get details of symptom
        /// </summary>
        /// <param name="symptomId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{symptomId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<SymptomResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Read([FromRoute] Guid symptomId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SymptomReadInputModel
            {
                SymptomId = symptomId
            }));
        }

        /// <summary>
        /// Update symptom
        /// </summary>
        /// <param name="symptomId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{symptomId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] Guid symptomId, [FromBody] SymptomUpdateModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SymptomUpdateInputModel
            {
                SymptomId = symptomId,
                Details = input
            }));
        }

        /// <summary>
        /// Delete symptom
        /// </summary>
        /// <param name="symptomId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{symptomId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] Guid symptomId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SymptomDeleteInputModel
            {
                SymptomId = symptomId
            }));
        }

        /// <summary>
        /// Set enabled symptom
        /// </summary>
        /// <param name="symptomId"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{symptomId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] Guid symptomId, [FromBody] SymptomSetEnabledInput input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SymptomSetEnabledInputModel
            {
                SymptomId = symptomId,
                IsEnabled = input.IsEnabled
            }));
        }

        /// <summary>
        /// Search all symptom of location by pages
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="keyword">Search by name</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<SymptomResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [Range(1, 100)] int pageSize = 50,
            [Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SymptomSearchInputModel()
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }
    }
}
