﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using Thucook.Core;
using Thucook.Main.ApiAction;
using Thucook.Main.ApiModel;
using Thucook.Main.ApiModel.ApiInputModels.Specialty;
using Thucook.Main.ApiModel.ApiReponseModels;

namespace Thucook.Main.API.Controllers
{
    [Authorize]
    [Route("specialties")]
    [ApiController]
    public class SpecialtyController : BaseController
    {
        public SpecialtyController(IMediator mediator,
            ICurrentContext currentContext
        ) : base(mediator, currentContext)
        {
        }

        /// <summary>
        /// Create new specialty
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] SpecialtyCreateInputModel input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, input));
        }

        /// <summary>
        /// Get details of specialty
        /// </summary>
        /// <param name="specialtyId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{specialtyId}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiResponseModel<SpecialtyResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Read([FromRoute] Guid specialtyId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SpecialtyReadInputModel
            {
                SpecialtyId = specialtyId
            }));
        }

        /// <summary>
        /// Update specialty
        /// </summary>
        /// <param name="specialtyId"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{specialtyId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] Guid specialtyId, [FromBody] SpecialtyUpdateModel details)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SpecialtyUpdateInputModel
            {
                SpecialtyId = specialtyId,
                Details = details
            }));
        }

        /// <summary>
        /// Delete specialty
        /// </summary>
        /// <param name="specialtyId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{specialtyId}")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] Guid specialtyId)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SpecialtyDeleteInputModel
            {
                SpecialtyId = specialtyId
            }));
        }

        /// <summary>
        /// Set enabled specialty
        /// </summary>
        /// <param name="specialtyId"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{specialtyId}/set-enabled")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> SetEnabled([FromRoute] Guid specialtyId, [FromBody] SpecialtySetEnabledInput input)
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SpecialtySetEnabledInputModel
            {
                SpecialtyId = specialtyId,
                IsEnabled = input.IsEnabled
            }));
        }

        /// <summary>
        /// Search all specialty of location by pages
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="keyword">Search by name</param>
        /// <returns></returns> 
        [HttpGet]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ApiPagingResponseModel<SpecialtyResponseModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Search(
            [Range(1, 100)] int pageSize = 50,
            [Range(1, int.MaxValue)] int pageNumber = 1,
            [StringLength(128)] string keyword = "")
        {
            return await _mediator.Send(ApiActionModel.CreateRequest(UserId, LocationId, new SpecialtySearchInputModel()
            {
                PageSize = pageSize,
                PageNumber = pageNumber,
                Keyword = keyword
            }));
        }
    }
}
