using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using NewRelic.LogEnrichers.Serilog;
using Serilog;
using System;
using System.IO;

namespace Thucook.Main.API
{
    public class Program
    {
        // dotnet ef dbcontext scaffold "server=localhost;port=3306;database=thrucook;uid=root;password=hung2001@;TreatTinyasBoolean=true" Pomelo.EntityFrameworkCore.MySql -c ThucookContext -f
        //public static void Main(string[] args)
        //{
        //    CreateHostBuilder(args).Build().Run();
        //}

        //public static IHostBuilder CreateHostBuilder(string[] args) =>
        //    Host.CreateDefaultBuilder(args)
        //        .ConfigureAppConfiguration(app =>
        //        {
        //            app.AddJsonFile("appsettings.json");
        //        })
        //        .ConfigureWebHostDefaults(webBuilder =>
        //        {
        //            webBuilder.UseStartup<Startup>();
        //        });
        public static int Main(string[] args)
        {
            var logFolder = Environment.GetEnvironmentVariable("LOG_FOLDER");
            if (string.IsNullOrEmpty(logFolder))
            {
                logFolder = AppContext.BaseDirectory;
            }
            logFolder = Path.Combine(logFolder, "api-logs");
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.Async(a => a.File(
                    Path.Combine(logFolder, "main.api..log"),
                    rollingInterval: RollingInterval.Day,
                    outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff}] [{Level}] {Message}{NewLine}{Exception}"
                ))
                .WriteTo.Async(a => a.File(
                    formatter: new NewRelicFormatter(),
                    Path.Combine(logFolder, "new-relic.main.api..json"),
                    rollingInterval: RollingInterval.Day
                ))
                .CreateLogger();
            try
            {
                var host = new WebHostBuilder()
                    .ConfigureAppConfiguration(app =>
                    {
                        app.AddJsonFile("appsettings.json");
                    })
                    .UseKestrel()
                    .UseUrls($"http://0.0.0.0:6001")
                    .UseStartup<Startup>()
                    .Build();

                Log.Information("Starting host...");
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly.");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
