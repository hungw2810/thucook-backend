﻿using System;
using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Schedule
{
    public class ScheduleUpdateInputModel : IApiInput
    {
        [Required]
        public long ScheduleId { get; set; }

        [Required]
        public ScheduleUpdateInput Details { get; set; }
    }

    public class ScheduleUpdateInput
    {
        [Required]
        public EditDoctorScheduleCaseEnum EditCaseType { get; set; }

        [Required]
        public EditDoctorScheduleApplyToEnum EditApplyTo { get; set; }

        public long OccurrenceStart { get; set; }

        [Required]
        public Guid DoctorId { get; set; }

        [Required]
        public long StartDateTimeUnix { get; set; }

        [Required]
        public long EndDateTimeUnix { get; set; }

        public string RecurrenceString { get; set; }
    }
}
