﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Schedule
{
    public class ScheduleCreateInputModel : IApiInput
    {
        [Required]
        public Guid DoctorId { get; set; }

        [Required]
        public long StartDateTimeUnix { get; set; }

        [Required]
        public long EndDateTimeUnix { get; set; }

        [MaxLength(2048)]
        public string RecurrenceString { get; set; }
    }
}
