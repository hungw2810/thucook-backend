﻿using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Schedule
{
    public class ScheduleDeleteInputModel : IApiInput
    {
        [Required]
        public long ScheduleId { get; set; }

        [Required]
        public ScheduleDeleteInput Details { get; set; }
    }

    public class ScheduleDeleteInput
    {
        [EnumDataType(typeof(EditDoctorScheduleApplyToEnum))]
        public EditDoctorScheduleApplyToEnum EditApplyTo { get; set; }

        public long OccurenceStart { get; set; }
    }
}
