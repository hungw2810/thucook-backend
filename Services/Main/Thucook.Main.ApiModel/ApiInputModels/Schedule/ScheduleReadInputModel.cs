﻿using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Schedule
{
    public class ScheduleReadInputModel : IApiInput
    {
        [Required]
        public long ScheduleId { get; set; }
    }
}
