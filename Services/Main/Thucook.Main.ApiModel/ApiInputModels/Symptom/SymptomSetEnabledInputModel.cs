﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Symptom
{
    public class SymptomSetEnabledInputModel : IApiInput
    {
        [Required]
        public Guid SymptomId { get; set; }

        [Required]
        public bool IsEnabled { get; set; }
    }
    public class SymptomSetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}
