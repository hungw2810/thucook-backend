﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Symptom
{
    public class SymptomDeleteInputModel : IApiInput
    {
        [Required]
        public Guid SymptomId { get; set; }
    }
}
