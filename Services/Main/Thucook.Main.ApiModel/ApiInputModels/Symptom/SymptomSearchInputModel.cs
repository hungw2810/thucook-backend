﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Symptom
{
    public class SymptomSearchInputModel : IApiInput
    {
        [Range(1, 100)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }

        [StringLength(128)]
        public string Keyword { get; set; }
    }
}
