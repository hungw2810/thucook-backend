﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Symptom
{
    public class SymptomUpdateInputModel : IApiInput
    {
        [Required]
        public Guid SymptomId { get; set; }

        [Required]
        public SymptomUpdateModel Details { get; set; }
    }

    public class SymptomUpdateModel
    {
        public Guid? SpecialtyId { get; set; }

        [Required]
        public string SymptomName { get; set; }

        [Required]
        public string Content { get; set; }
    }
}
