﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Specialty
{
    public class SpecialtyReadInputModel : IApiInput
    {
        [Required]
        public Guid SpecialtyId { get; set; }
    }
}
