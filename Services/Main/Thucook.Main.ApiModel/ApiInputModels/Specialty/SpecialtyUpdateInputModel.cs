﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Specialty
{
    public class SpecialtyUpdateInputModel : IApiInput
    {
        [Required]
        public Guid SpecialtyId { get; set; }

        public SpecialtyUpdateModel Details { get; set; }
    }

    public class SpecialtyUpdateModel
    {
        [Required]
        public string SpecialtyName { get; set; }

        public string SpecialtyShortName { get; set; }
    }
}
