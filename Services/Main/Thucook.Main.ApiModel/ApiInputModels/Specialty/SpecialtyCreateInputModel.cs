﻿using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Specialty
{
    public class SpecialtyCreateInputModel : IApiInput
    {
        [Required]
        [StringLength(128)]
        public string SpecialtyName { get; set; }

        public string SpecialtyShortName { get; set; }
    }
}
