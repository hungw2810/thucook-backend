﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Specialty
{
    public class SpecialtyDeleteInputModel : IApiInput
    {
        [Required]
        public Guid SpecialtyId { get; set; }
    }
}
