﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Specialty
{
    public class SpecialtySetEnabledInputModel : IApiInput
    {
        [Required]
        public Guid SpecialtyId { get; set; }

        [Required]
        public bool IsEnabled { get; set; }
    }

    public class SpecialtySetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}
