﻿using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Medicine
{
    public class MedicineReadInputModel : IApiInput
    {
        [Required]
        public long MedicineId { get; set; }
    }
}

