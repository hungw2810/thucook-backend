﻿using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Medicine
{
    public class MedicineUpdateInputModel : IApiInput
    {
        [Required]
        public long MedicineId { get; set; }

        [Required]
        public MedicineUpdateInput Details { get; set; }
    }

    public class MedicineUpdateInput
    {
        [Required]
        [StringLength(256)]
        public string MedicineName { get; set; }

        [Required]
        public MedicineUnitTypeEnum MedicineUnitTypeId { get; set; }

        [Required]
        public float Quantity { get; set; }
    }
}