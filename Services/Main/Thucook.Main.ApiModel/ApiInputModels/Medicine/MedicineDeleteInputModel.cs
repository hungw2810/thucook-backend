﻿using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Medicine
{
    public class MedicineDeleteInputModel : IApiInput
    {
        [Required]
        public long MedicineId { get; set; }
    }
}
