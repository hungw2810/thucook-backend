﻿using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Medicine
{
    public class MedicineCreateInputModel : IApiInput
    {
        [Required]
        [StringLength(256)]
        public string MedicineName { get; set; }

        [Required]
        public MedicineUnitTypeEnum MedicineUnitTypeId { get; set; }

        [Required]
        public float Quantity { get; set; }
    }
}
