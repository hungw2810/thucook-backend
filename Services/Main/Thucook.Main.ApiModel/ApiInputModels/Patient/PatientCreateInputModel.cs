﻿using System;
using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Patient
{
    public class PatientCreateInputModel : IApiInput
    {
        [Required]
        [StringLength(256)]
        public string FullName { get; set; }

        [Required]
        public long BirthdayUnixTime { get; set; }

        [Required]
        public GenderEnum Gender { get; set; }

        [Required]
        [StringLength(128)]
        public string PhoneNumber { get; set; }

        [Required]
        [StringLength(256)]
        public string Address { get; set; }

        [Required]
        [StringLength(128)]
        public string Email { get; set; }

        [Range(0, short.MaxValue)]
        public short? HeightInCm { get; set; }

        [Range(0, short.MaxValue)]
        public short? WeightInKg { get; set; }

        [StringLength(1024)]
        public string Allergy { get; set; }
    }
}

