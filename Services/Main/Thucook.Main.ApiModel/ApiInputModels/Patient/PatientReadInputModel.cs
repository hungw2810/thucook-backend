﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Patient
{
    public class PatientReadInputModel : IApiInput
    {
        [Required]
        public Guid PatientId { get; set; }
    }
}

