﻿
using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Patient
{
    public class PatientSearchInputModel : IApiInput
    {
        [Range(1, 100)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }

        public PatientsSearchInput Details { get; set; }
    }
    public class PatientsSearchInput
    {
        [StringLength(128)]
        public string Keyword { get; set; }

        public string PatientCode { get; set; }

        public GenderEnum? Gender { get; set; }

        public int YearOfBirth { get; set; }
    }
}

