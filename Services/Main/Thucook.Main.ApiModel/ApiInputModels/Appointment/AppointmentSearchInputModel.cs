﻿using System;
using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Appointment
{
    public class AppointmentSearchInputModel : IApiInput
    {
        [Range(1, 100)]
        public int PageSize { get; set; }

        [Range(1, int.MaxValue)]
        public int PageNumber { get; set; }

        public AppointmentSearchInput Details { get; set; }
    }
    public class AppointmentSearchInput
    {
        [StringLength(128)]
        public string Keyword { get; set; }

        public Guid? SpecialtyId { get; set; }

        public Guid? DoctorId { get; set; }

        public AppointmentStatusEnum? AppointmentStatusId { get; set; }

        [Required]
        public long FromDate { get; set; }

        [Required]
        public long ToDate { get; set; }
    }
}

