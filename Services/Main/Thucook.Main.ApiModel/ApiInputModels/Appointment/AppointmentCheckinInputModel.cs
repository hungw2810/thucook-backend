﻿using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Appointment
{
    public class AppointmentCheckinInputModel : IApiInput
    {
        [Required]
        public long AppointmentId { get; set; }
    }
}
