﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Appointment
{
    public class AppointmentCreateInputModel : IApiInput
    {
        [Required]
        public Guid PatientId { get; set; }

        [Required]
        public Guid LocationId { get; set; }

        [Required]
        public Guid DoctorId { get; set; }

        [Required]
        public long ScheduleId { get; set; }

        [Required]
        public long StartDateTimeUnix { get; set; }

        public string Symptom { get; set; }
    }
}
