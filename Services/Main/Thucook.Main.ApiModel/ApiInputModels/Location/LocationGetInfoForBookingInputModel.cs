﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Location
{
    public class LocationGetInfoForBookingInputModel : IApiInput
    {
        [Required]
        public Guid LocationId { get; set; }
    }
}
