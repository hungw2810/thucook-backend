﻿using System;
using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.User
{
    public class UserUpdateInfoInputModel : IApiInput
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public UserUpdateInfoModel Details { get; set; }
    }

    public class UserUpdateInfoModel
    {
        [Required]
        [StringLength(128)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(128)]
        public string LastName { get; set; }

        [Required]
        [StringLength(128)]
        public string Email { get; set; }

        [Required]
        [StringLength(128)]
        public string PhoneNumber { get; set; }

        [Required]
        public long? BirthDayUnixTime { get; set; }

        [Required]
        public GenderEnum Gender { get; set; }

        [Required]
        public CityEnum CityId { get; set; }

        [Required]
        [StringLength(256)]
        public string Address { get; set; }
    }
}

