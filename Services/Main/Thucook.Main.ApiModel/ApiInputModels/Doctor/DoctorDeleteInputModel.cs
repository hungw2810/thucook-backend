﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Doctor
{
    public class DoctorDeleteInputModel : IApiInput
    {
        [Required]
        public Guid DoctorId { get; set; }
    }
}
