﻿using System;

namespace Thucook.Main.ApiModel.ApiInputModels.Doctor
{
    public class DoctorReadInputModel : IApiInput
    {
        public Guid DoctorId { get; set; }
    }
}
