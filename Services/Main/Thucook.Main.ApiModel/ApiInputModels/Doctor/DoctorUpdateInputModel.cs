﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Doctor
{
    public class
        DoctorUpdateInputModel : IApiInput
    {
        [Required]
        public Guid DoctorId { get; set; }

        [Required]
        public DoctorUpdateModel Details { get; set; }
    }
    public class DoctorUpdateModel
    {
        [MaxLength(128)]
        public string DoctorCode { get; set; }

        [Required]
        [MaxLength(128)]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(128)]
        public string Email { get; set; }

        public short? YearsOfExperience { get; set; }

        public Guid? SpecialtyId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public short TimePerAppointmentInMinutes { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public short BufferTimePerAppointmentInMinutes { get; set; }

        [Required]
        public bool IsVisibleForBooking { get; set; }

        [Required]
        public bool IsEnabled { get; set; }
    }
}
