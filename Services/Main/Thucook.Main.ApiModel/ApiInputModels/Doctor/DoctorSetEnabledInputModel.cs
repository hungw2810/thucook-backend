﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Thucook.Main.ApiModel.ApiInputModels.Doctor
{
    public class DoctorSetEnabledInputModel : IApiInput
    {
        [Required]
        public Guid DoctorId { get; set; }

        [Required]
        public bool IsEnabled { get; set; }
    }

    public class DoctorSetEnabledInput
    {
        [Required]
        public bool IsEnabled { get; set; }
    }
}
