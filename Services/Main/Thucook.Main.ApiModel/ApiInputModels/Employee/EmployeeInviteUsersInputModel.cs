﻿using System.ComponentModel.DataAnnotations;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiInputModels.Employee
{
    public class EmployeeInviteUsersInputModel : IApiInput
    {
        [Required]
        [MaxLength(20)]
        public string Email { get; set; }

        [Required]
        public RoleTypeEnum RoleId { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
