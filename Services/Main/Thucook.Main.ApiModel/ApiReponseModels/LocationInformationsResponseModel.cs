﻿using System;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class LocationInformationsResponseModel : IApiResponseData
    {
        public Guid LocationId { get; set; }

        public string LocationName { get; set; }

        public LocationInfoSpecialtyResponseModel[] Specialties { get; set; }

        public LocationInfoDoctorResponseModel[] Doctors { get; set; }

        public LocationInfoSymptomResponseModel[] Symptoms { get; set; }
    }

    public class LocationInfoSpecialtyResponseModel
    {
        public Guid SpecialtyId { get; set; }

        public string SpecialtyName { get; set; }
    }

    public class LocationInfoDoctorResponseModel
    {
        public Guid DoctorId { get; set; }

        public string DoctorName { get; set; }

        public Guid? SpecialtyId { get; set; }
    }

    public class LocationInfoSymptomResponseModel
    {
        public Guid SymptomId { get; set; }

        public string SymptomName { get; set; }

        public string Content { get; set; }

        public Guid? SpecialtyId { get; set; }
    }
}
