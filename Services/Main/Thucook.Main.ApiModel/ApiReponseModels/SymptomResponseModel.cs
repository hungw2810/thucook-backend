﻿using System;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class SymptomResponseModel : IApiResponseData
    {
        public Guid SymptomId { get; set; }

        public Guid? SpecialtyId { get; set; }

        public string SpecialtyName { get; set; }

        public string SymptomName { get; set; }

        public string Content { get; set; }

        public bool IsEnabled { get; set; }
    }
}
