﻿using System;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class UserInfoResponseModel : IApiResponseData
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public long? BirthdayUnix { get; set; }

        public GenderEnum Gender { get; set; }

        public CityEnum? CityId { get; set; }

        public string Address { get; set; }
    }
}
