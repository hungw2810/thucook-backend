﻿using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class AppointmentReponseModel : IApiResponseData
    {
        public long AppointmentId { get; set; }
        public PatientResponseModel Patient { get; set; }
        public string DoctorName { get; set; }
        public AppointmentStatusEnum AppointmentStatusId { get; set; }
        public long StartDatetimeUnix { get; set; }
    }
}

