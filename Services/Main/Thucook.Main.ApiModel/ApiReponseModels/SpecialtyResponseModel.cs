﻿using System;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class SpecialtyResponseModel : IApiResponseData
    {
        public Guid SpecialtyId { get; set; }

        public string SpecialtyName { get; set; }

        public string SpecialtyShortName { get; set; }

        public bool IsEnabled { get; set; }
    }
}
