﻿using System;
using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class PatientResponseModel : IApiResponseData
    {
        public Guid PatientId { get; set; }

        public string FullName { get; set; }

        public long BirthdayUnix { get; set; }

        public GenderEnum Gender { get; set; }

        public Guid? UserId { get; set; }

        public bool IsDefault { get; set; }

        public string PatientCode { get; set; }

        public string PhoneNumber { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public short? HeightInCm { get; set; }

        public short? WeightInKg { get; set; }

        public string Allergy { get; set; }
    }
}
