﻿using Thucook.Commons.Enums;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class MedicineResponseModel : IApiResponseData
    {
        public long MedicineId { get; set; }

        public string MedicineName { get; set; }

        public MedicineUnitTypeEnum MedicineUnitTypeId { get; set; }

        public float Quantity { get; set; }

        public bool IsEditable { get; set; }
    }
}
