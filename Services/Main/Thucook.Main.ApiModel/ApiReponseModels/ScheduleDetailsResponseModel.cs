﻿using System;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class ScheduleDetailsResponseModel : IApiResponseData
    {
        public long ScheduleId { get; set; }

        public DoctorBaseResponseModel Doctor { get; set; }

        public string LocationName { get; set; }

        public long StartDateTimeUnix { get; set; }

        public long EndDateTimeUnix { get; set; }

        public string RecurrenceString { get; set; }

        public int SlotTimeSpan { get; set; }
    }
}
