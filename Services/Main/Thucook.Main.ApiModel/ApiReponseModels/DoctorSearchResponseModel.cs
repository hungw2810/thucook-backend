﻿using System;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class DoctorSearchResponseModel : IApiResponseData
    {
        public Guid DoctorId { get; set; }

        public string DoctorCode { get; set; }

        public string DoctorName { get; set; }

        public bool IsEnabled { get; set; }

        public string SpecialtyName { get; set; }

        public string SpecialtyShortName { get; set; }
    }
}
