﻿using System;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class DoctorResponseModel : DoctorBaseResponseModel
    {
        public bool IsEnabled { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public short? YearsOfExperience { get; set; }

        public Guid? SpecialtyId { get; set; }

        public string SpecialtyName { get; set; }

        public short TimePerAppointmentInMinutes { get; set; }

        public short BufferTimePerAppointmentInMinutes { get; set; }

        public bool IsVisibleForBooking { get; set; }
    }
}
