﻿using System;

namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class DoctorBaseResponseModel : IApiResponseData
    {
        public Guid DoctorId { get; set; }

        public string DoctorCode { get; set; }

        public string DoctorName { get; set; }
    }
}
