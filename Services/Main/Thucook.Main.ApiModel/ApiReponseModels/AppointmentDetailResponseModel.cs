﻿namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class AppointmentDetailResponseModel : IApiResponseData
    {
        public long AppointmentId { get; set; }

        public string Symptom { get; set; }

        public string LocationName { get; set; }

        public string LocationAddress { get; set; }

        public string DoctorName { get; set; }

        public int AppointmentCode { get; set; }

        public short AppointmentStatusId { get; set; }

        public long StartDatetimeUnix { get; set; }

        public PatientResponseModel Patient { get; set; }
    }
}
