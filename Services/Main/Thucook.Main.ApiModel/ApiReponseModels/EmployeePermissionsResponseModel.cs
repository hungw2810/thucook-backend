﻿namespace Thucook.Main.ApiModel.ApiReponseModels
{
    public class EmployeePermissionsResponseModel : IApiResponseData
    {
        public short RoleId { get; set; }
    }
}
