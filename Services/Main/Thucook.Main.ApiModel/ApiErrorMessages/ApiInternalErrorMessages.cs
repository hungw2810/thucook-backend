﻿namespace Thucook.Main.ApiModel.ApiErrorMessages
{
    /// <summary>
    /// Prefix must be: PINT_
    /// </summary>
    public static class ApiInternalErrorMessages
    {

        #region Exact errors PINT_1xxx
        public static ApiErrorMessage EmailAlreadyUsed => new ApiErrorMessage
        {
            Code = "PINT_1001",
            Value = "Email Already Used"
        };

        public static ApiErrorMessage DuplicateLocationName => new ApiErrorMessage
        {
            Code = "PINT_1002",
            Value = "Duplicate Location Name"
        };

        public static ApiErrorMessage DuplicateLocationPhoneNumber => new ApiErrorMessage
        {
            Code = "PINT_1003",
            Value = "Duplicate Location Phone Number"
        };

        public static ApiErrorMessage DuplicateName => new ApiErrorMessage
        {
            Code = "PINT_1004",
            Value = "Duplicate Name"
        };

        public static ApiErrorMessage DuplicateShortName => new ApiErrorMessage
        {
            Code = "PINT_1005",
            Value = "Duplicate Short Name"
        };

        public static ApiErrorMessage DuplicateDoctorCode => new ApiErrorMessage
        {
            Code = "PINT_1006",
            Value = "Duplicate Doctor Code"
        };

        public static ApiErrorMessage PastTimeNotAllowed => new ApiErrorMessage
        {
            Code = "PINT_1007",
            Value = "Past Time Not Allowed"
        };

        public static ApiErrorMessage ScheduleDurationTooLong => new ApiErrorMessage
        {
            Code = "PINT_1008",
            Value = "Schedule Duration Too Long"
        };

        public static ApiErrorMessage ScheduleOverlap => new ApiErrorMessage
        {
            Code = "PINT_1009",
            Value = "Schedule Over lap"
        };

        public static ApiErrorMessage TooMuchAppointmentsOnSameDay => new ApiErrorMessage
        {
            Code = "PINT_1010",
            Value = "Too Much Appointments On Same Day"
        };

        public static ApiErrorMessage TooMuchAppointmentsAtSameTime => new ApiErrorMessage
        {
            Code = "PINT_1011",
            Value = "Too Much Appointments At Same Time"
        };

        public static ApiErrorMessage DuplicateInternalName => new ApiErrorMessage
        {
            Code = "PINT_1012",
            Value = "Duplicate Internal Name"
        };

        public static ApiErrorMessage AppointmentStatusNotAllow => new ApiErrorMessage
        {
            Code = "PINT_1013",
            Value = "Appointment Status Not Allow"
        };

        public static ApiErrorMessage DuplicatePatientFullName => new ApiErrorMessage
        {
            Code = "PINT_1014",
            Value = "Trùng tên hồ sơ bệnh nhân"
        };
        #endregion

        #region Notfound errors PINT_4xxx
        public static ApiErrorMessage SpecialtyNotFound => new ApiErrorMessage
        {
            Code = "PINT_4001",
            Value = "Specialty Not Found"
        };

        public static ApiErrorMessage SymptomNotFound => new ApiErrorMessage
        {
            Code = "PINT_4002",
            Value = "Symptom Not Found"
        };

        public static ApiErrorMessage DoctorNotFound => new ApiErrorMessage
        {
            Code = "PINT_4003",
            Value = "Doctor Not Found"
        };

        public static ApiErrorMessage PatientNotFound => new ApiErrorMessage
        {
            Code = "PINT_4004",
            Value = "Không tìm thấy hồ sơ bệnh nhân"
        };

        public static ApiErrorMessage LocationNotFound => new ApiErrorMessage
        {
            Code = "PINT_4005",
            Value = "Cơ sở không tồn tại"
        };

        public static ApiErrorMessage MedicineNotFound => new ApiErrorMessage
        {
            Code = "PINT_4006",
            Value = "Dược phẩm không tồn tại"
        };

        public static ApiErrorMessage ScheduleNotFound => new ApiErrorMessage
        {
            Code = "PINT_4007",
            Value = "Schedule Not Found"
        };

        public static ApiErrorMessage AppointmentNotFound => new ApiErrorMessage
        {
            Code = "PINT_4008",
            Value = "Appointment Not Found"
        };

        public static ApiErrorMessage EmployeeNotFound => new ApiErrorMessage
        {
            Code = "PINT_4009",
            Value = "Employee Not Found"
        };

        public static ApiErrorMessage RoleNotFound => new ApiErrorMessage
        {
            Code = "PINT_4010",
            Value = "Role Not Found"
        };

        public static ApiErrorMessage UserNotFound => new ApiErrorMessage
        {
            Code = "PINT_4011",
            Value = "Nugời dùng không tồn tại"
        };
        #endregion

        #region Invalid errors PINT_5xxx
        public static ApiErrorMessage InvalidSignupCode => new ApiErrorMessage
        {
            Code = "PINT_5001",
            Value = "Signup code không hợp lệ"
        };

        public static ApiErrorMessage InvalidUserNameOrPassword => new ApiErrorMessage
        {
            Code = "PINT_5002",
            Value = "Tên đăng nhập hoặc mật khẩu không hợp lệ"
        };

        public static ApiErrorMessage InvalidAppointmentTime => new ApiErrorMessage
        {
            Code = "PINT_5003",
            Value = "Invalid Appointment Time"
        };

        public static ApiErrorMessage InvalidRecurrenceString => new ApiErrorMessage
        {
            Code = "PINT_5004",
            Value = "Invalid Recurrence String"
        };

        public static ApiErrorMessage InvalidValueType => new ApiErrorMessage
        {
            Code = "PINT_5005",
            Value = "Invalid Value Type"
        };

        public static ApiErrorMessage InvalidEmail => new ApiErrorMessage
        {
            Code = "PINT_5006",
            Value = "Invalid Email"
        };
        #endregion
    }
}
