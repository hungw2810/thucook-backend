﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Thucook.EntityFramework
{
    public partial class Location
    {
        public Location()
        {
            Appointments = new HashSet<Appointment>();
            Doctors = new HashSet<Doctor>();
            Employees = new HashSet<Employee>();
            Medicines = new HashSet<Medicine>();
            Specialties = new HashSet<Specialty>();
            Symptoms = new HashSet<Symptom>();
        }

        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public short CityId { get; set; }
        public string Address { get; set; }
        public double? MinPrice { get; set; }
        public double? MaxPrice { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? UpdatedByUserId { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<Appointment> Appointments { get; set; }
        public virtual ICollection<Doctor> Doctors { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public virtual ICollection<Medicine> Medicines { get; set; }
        public virtual ICollection<Specialty> Specialties { get; set; }
        public virtual ICollection<Symptom> Symptoms { get; set; }
    }
}
