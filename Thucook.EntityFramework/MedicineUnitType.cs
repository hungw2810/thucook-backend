﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Thucook.EntityFramework
{
    public partial class MedicineUnitType
    {
        public MedicineUnitType()
        {
            Medicines = new HashSet<Medicine>();
        }

        public short MedicineUnitTypeId { get; set; }
        public string MedicineUnitTypeName { get; set; }

        public virtual ICollection<Medicine> Medicines { get; set; }
    }
}
